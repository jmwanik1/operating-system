#pragma once

#include <iostream>
#include <thread>         
#include <mutex>    
#include <queue>
#include <unistd.h>

#include "./headers/cpu/cpu.h" 
#include "./headers/cpu/pcb/pcb.h"
#include "./headers/helpers/conversions.h"

using namespace Conversions;

struct Dispatcher
{
	public:
		std::queue<PCB*> io_queue;         // Queue for io servicing
		std::vector<PCB> page_fault_queue; // Queue for page fault servicing
		std::vector<std::string> finished_jobs_queue; // finished jobs
	private:
		std::vector<CPU*> processors;
		CPU* cpu;
		const unsigned int num_of_cpu = 4; 
		std::mutex mtx;           // protects reads in ram when in cpu
		std::mutex mtx_rd_queue;  // protects read queue when dispatching
		std::mutex clean;           // protects reads in ram when in cpu
		std::mutex mtx_write;
		std::string accumulator;  // stores result after thread exits
		std::string job_id;       // Program bound for cpu exec
		// processors
		//Do we need mtx.unlock as the first line of Page fault handler. --DCC
		//I didn't put it there so I didn't know. --DCC
	public: 
		Memory& memory;
		std::vector<std::thread> threads;
		std::map<int, int> program_on_thread;
		
		Dispatcher(Memory &memory) : 
		memory(memory)
		{
			init();
		}
	
		void init()
		{
			clear_processors();
			for (unsigned int i = 0; i < num_of_cpu; i++)
			{
				cpu = new CPU(memory, mtx, 
											accumulator, job_id, 
											io_queue, page_fault_queue, finished_jobs_queue);
				cpu->init_instructions();
				processors.push_back(cpu);
				program_on_thread[i] = 0;
			}
		}
		
		void dispatch()
		{
			for(unsigned int i = 0; i < num_of_cpu; i++)
				threads.emplace_back(std::thread(&Dispatcher::run,this, i));
			
			usleep(100);
			
			
			for(auto& thread : threads)
				thread.join();
			std::vector<std::thread>().swap(threads);
		}
		
		void run(unsigned int cpu_no)
		{ 
		  std::lock_guard<std::mutex> lck (mtx);
			CPU& processor = *processors.at(cpu_no);
			if(!memory.ready_queue().empty() && !processor.isBusy())
			{
				PCB &next = processor.memory.ready_queue().dequeue();
				program_on_thread[cpu_no] = hex_to_decimal(next.job_id);
				// Check the process is ready for the cpu
				if (next.state.ready)
					processor.run(next);
				processor.not_busy();
			}
		}
		
		void service_queues()
		{
			std::cout << "Servicing..." << std::endl;
			std::cout << "Number of queues: " << memory.num_of_queues() << std::endl;
			std::cout << "Completed servicing..." << std::endl;
		}
		
		bool page_fault_handler(PCB* process)
		{
			std::cout << "====================================" << std::endl;
			std::cout << "Handling page fault..." << std::endl;
			
			int pages_to_retrieve = 
				process->state.missing_frame - (process->frames_assigned - 1);
			std::cout << "Instruction size: " << process->instructions_size() << std::endl;
			std::cout << "Missing frame: " << process->state.missing_frame << std::endl;
			std::cout << "Missing address: " << process->state.missing_address << std::endl;
			std::cout << "Total frames assigned: " << process->frames_assigned << std::endl;
			std::cout << "Pages to retrieve: " << pages_to_retrieve << std::endl;
			std::cout << "Entering Size of cache: " << process->state.cache.size() << std::endl;
			std::cout << "Total frames read: " << process->frames_assigned << std::endl;
			
			process->state.set_ready();
			bool is_successful = true;
			// What to do
			// 1. base of frames not allocated
			unsigned int page_table_id = process->frames_assigned;
			unsigned int start = process->frames_assigned;
			
			if (pages_to_retrieve == 0)
        pages_to_retrieve += 1;
    
			for (unsigned int i = page_table_id; i < start + pages_to_retrieve; i++)
			{
				// 2. retrieve a free frame
				int empty_frame = memory.memory_manager.ram.get_free_frame();
				std::cout << "Frame retrieved: " << empty_frame << std::endl;
				// 3. Get page from disk
				if (empty_frame != -1)
				{
					int page_number = process->page_table.page_number(i);
					std::cout << "Page num: " << page_number << std::endl;
					std::cout << "Index: " << i << std::endl;
					std::vector<std::string> page = memory.memory_manager.disk.retrieve(page_number);
					// ^^^^ this is the problem 
					// 4. Transfer page to ram
					memory.memory_manager.ram.store_insert(page, empty_frame);
					// 5. Frame is not empty/free anymore
					memory.memory_manager.ram.free_frame(empty_frame);
					// 6. Update pcb
						// 6.1 Update page table
					process->page_table.insert(i, empty_frame, true);
						// 6.2 Update pcb entries
					process->frames_assigned += 1;
				}
				else
				{
					is_successful = false;
					break;
				}
				std::cout << "Page table id: " << i << " Frame: "<< empty_frame<< std::endl;
			}
			
			// add process into the ready queue
			int job_id = hex_to_decimal(process->job_id);
			if (!memory.last_queue().job_exists(job_id) && is_successful)
				memory.last_queue().enqueue(*process); 
				
			/* std::string fname = "cache:" + (std::to_string(hex_to_decimal(process->job_id))) + ".txt";
			process->state.cache.print(fname, hex_to_decimal(process->job_id));  */
			process->page_table.print();
			std::cout << "Finished Handling page fault..." << std::endl; 
			std::cout << "====================================" << std::endl;
			return is_successful;
		}
		
		void io_servicing(PCB* process)
		{
			std::cout << "Servicing..." << std::endl;
			std::string opcode = binary_to_hex(process->state.registers.opcode);
			if (opcode == "01")
			{
				write(process);
			}
			// memory.ready_queue().enqueue(*process);
			std::cout << "Completed servicing..." << std::endl;
		}
		
		
		void write(PCB* process)
		{
			unsigned int eff_address = effective_address(process->state.registers.address);
			// write to disk 
			unsigned int page_number = eff_address / 4;
			if (!(eff_address % 4 == 0))
				page_number += 1;
			page_number += process->start_page_in_disk;
			unsigned int offset = 4 - (eff_address % 4);
			//mtx.lock();
			memory.memory_manager.disk.store_write(page_number, offset,
						process->state.registers.reg[process->state.registers.in_out_r1]);
			//mtx.unlock();
		}
		
		void shutdown()
		{
			std::cout << "Shutting down..." << std::endl;
			std::cout << "Ready queue size: " << memory.ready_queue().size() << std::endl;
			std::cout << processors.size() << std::endl;
			clear_processors();
			
			std::cout << "Shutdown completed..." << std::endl; 
		}
		
		std::vector<PCB> &fault_queue()
		{
			return page_fault_queue;
		}
		
		void clear_processors()
		{
			for (auto& processor:processors)
					delete processor;
		}
		
		void insert_to_page_fault_queue(std::vector<PCB>& jobs)
		{
			for (auto pcb : jobs)
			{
			  //mtx.lock();
				page_fault_queue.push_back(pcb);
				//mtx.unlock();
			}
		}
		
		void clear_page_fault_queue()
		{
			std::vector<PCB>().swap(page_fault_queue);
		}
		
		bool io_queue_empty() { return io_queue.empty(); }
		bool pfault_queue_empty() { return page_fault_queue.empty(); }
		
		int effective_address(std::string address)
		{
			return binary_to_decimal(address) / 4;
		}
		
		void print_result(unsigned int cpu_no)
		{
			std::cout << "******************************" << std::endl;
			std::cout << "Processor: "<<(cpu_no+1) <<"\t " << "Program: " << 
									program_on_thread[cpu_no] << std::endl;
									
			std::cout << "******************************" << std::endl;
		}
		
		
};
