#pragma once

#include "./headers/cpu/registers.h"
#include "./headers/memory/storage.h" 
// record of environment that is saved on interrupt
// including the pc, registers, permissions, buffers, caches, active
// pages/blocks
struct State 
{
  
  State() { /*default ctor */ } 
  //
  Storage cache;
  Registers registers;
  int program_counter = 0; 
  unsigned int page_number_fault = 0;
  unsigned int missing_address = 0;
  unsigned int missing_frame = 0;
  // States
  bool new_pcb = true; // default state
  bool ready = false;
  bool running = false;
  bool waiting = false;
  bool terminated = false;
  
  void set_program_counter(int pc) { program_counter = pc; }
  int get_program_counter() { return program_counter; }
  
  void set_registers(Registers reg) { registers = reg; }
  Registers get_registers() { return registers; }
  
  void set_cache(Storage cache) 
  {
    this->cache = cache;
  }
  
  void set_ready()
  {
    falsify_all();
    ready = true;
  }
  
  void set_running()
  {
    falsify_all();
    running = true;
  }
  
  void set_waiting()
  {
    falsify_all();
    waiting = true;
  }
  
  void set_terminated()
  {
    falsify_all();
    terminated = true;
  }
  
  void show_status()
  {
    std::cout << "New: " << new_pcb << std::endl;
    std::cout << "Ready: " << ready << std::endl;
    std::cout << "Running: " << running << std::endl;
    std::cout << "Waiting: " << waiting << std::endl;
    std::cout << "Terminated: " << terminated << std::endl;
  }
  
  private: 
    void falsify_all() 
    {
      terminated = false;
      running = false;
      ready = false;
      waiting = false;
    }
};