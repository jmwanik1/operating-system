#pragma once

#include "./headers/helpers/conversions.h"
#include "./headers/cpu/pcb/accounts.h"
#include "./headers/cpu/pcb/memories.h"
#include "./headers/cpu/registers.h"
#include "./headers/cpu/pcb/resources.h"
#include "./headers/cpu/pcb/scheduler.h"
#include "./headers/cpu/pcb/state.h"
#include "./headers/cpu/pcb/page_table/page_table.h"

struct PCB
{
  std::string job_id = "0x0";
  unsigned int cpu_id;
  bool new_pcb = true;
  // addresses in Disk and Ram
  unsigned int start_address_in_disk = 0;
  unsigned int end_address_in_disk = 0;
  unsigned int start_address_in_ram = 0;
  unsigned int program_counter = 0;
  // pages in disk 
  unsigned int total_pages = 0;
  unsigned int total_instruction_pages = 0;
  unsigned int start_page_in_disk = 0;
  unsigned int data_start_page_in_disk = 0;
  unsigned int end_page_in_disk = 0;
  // frames in ram
  unsigned int total_frames = 0;
  unsigned int total_instruction_frames = 0;
  unsigned int start_frame_in_ram = 0;
  unsigned int data_start_frame_in_ram = 0;
  unsigned int end_frame_in_ram = 0;
  unsigned int frames_assigned = 0;
  int retrieved_frames = 0;
  // Missing frame and address
  unsigned int missing_frame = 0;
  unsigned int missing_address = 0;
  // Instructions properties 
  std::string code_size;
  std::string priority;
  // Data properties
  std::string input_buffer_size;
  std::string output_buffer_size;
  std::string temp_buffer_size;
  // In Cache
  unsigned int start_in_cache = 0;
  unsigned int end_in_cache;
  unsigned int inp_buff_start;
  unsigned int out_buff_start;
  unsigned int temp_buff_start;
  
  Accounts accounts;
  Memories memories;           
  Registers registers;        
  Resources resources;
  Scheduler scheduler;
  State state; 
  PageTable page_table;

  unsigned int instructions_size() 
  {
    return Conversions::hex_to_decimal(code_size);
  }
  
  int data_size()
  {
    return Conversions::hex_to_decimal(input_buffer_size) 
            + Conversions::hex_to_decimal(output_buffer_size) 
            + Conversions::hex_to_decimal(temp_buffer_size);
  }
  
  int total_size()
  {
    return instructions_size() + data_size();
  }
  
  int inbuff_start_address_in_ram() 
  { 
    return start_address_in_ram 
          + Conversions::hex_to_decimal(code_size); 
  }
  
  int outbuff_start_address_in_ram() 
  { 
    return inbuff_start_address_in_ram() 
          + Conversions::hex_to_decimal(input_buffer_size); 
  }
  
  int tempbuff_start_address_in_ram() 
  { 
    return outbuff_start_address_in_ram() 
          + Conversions::hex_to_decimal(output_buffer_size); 
  }
  
  
  int end_address_in_ram()
  {
    return start_address_in_ram 
          + Conversions::hex_to_decimal(code_size) 
          + Conversions::hex_to_decimal(input_buffer_size) 
          + Conversions::hex_to_decimal(output_buffer_size) 
          + Conversions::hex_to_decimal(temp_buffer_size) - 1;
  }
  
  bool operator <(const PCB& pcb) const
  {
    return (Conversions::hex_to_decimal(priority) 
          < Conversions::hex_to_decimal(pcb.priority));
  }
  
  bool short_job(const PCB& pcb) const
  {
    return (Conversions::hex_to_decimal(code_size) 
          < Conversions::hex_to_decimal(pcb.code_size));
  }
  
  void print()
  {
    std::cout << "==== " << Conversions::hex_to_decimal(job_id) << " ====" << std::endl;
    std::cout << "code size: " << Conversions::hex_to_decimal(code_size) << std::endl;
    std::cout << "priority: " << Conversions::hex_to_decimal(priority) << std::endl;
    std::cout << "total_pages: " << total_pages << std::endl;
    std::cout << "start_page_in_disk: " << start_page_in_disk << std::endl;
    std::cout << "data_start_page_in_disk: " << data_start_page_in_disk << std::endl;
    std::cout << "end_page_in_disk: " << end_page_in_disk << std::endl;
    std::cout << "start_frame_in_ram: " << start_frame_in_ram << std::endl;
    std::cout << "data_start_frame_in_ram: " << data_start_frame_in_ram << std::endl;
    std::cout << "frames_assigned: " << frames_assigned << std::endl;
  }
};
