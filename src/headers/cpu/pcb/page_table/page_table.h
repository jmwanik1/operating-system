#pragma once

#include <cassert>
#include <map>
#include "./headers/cpu/pcb/page_table/entry.h" 

struct PageTable 
{
  std::map<int,Entry> table;
  unsigned int fault_address = 0;

  void insert(int i, int j, int k, bool valid)
  {
    Entry e;
    e.set_frame_number(j);
    e.set_page_number(k);
    e.set_valid(valid);
    table.insert(std::pair<int,Entry>(i, e));
  }
  
  void insert(int page_table_id, int frame_number, bool valid)
  {
    assert(page_table_id < table.size());
    table[page_table_id].set_frame_number(frame_number);
    table[page_table_id].set_valid(valid);
  }
  
  int size() { return table.size(); }
  
  int frame_number(unsigned int page_table_id)
  {
    return table[page_table_id].get_frame_number();
  }
  
  int page_number(unsigned int page_table_id)
  {
    return table[page_table_id].get_page_number();
  }
  
  bool valid(unsigned int page_table_id) 
  {
    return table[page_table_id].isValid();
  }
  
  void set_retrieved(int page_table_id, bool r)
  {
    assert(page_table_id < table.size());
    table[page_table_id].set_retrieved(r);
  }
  
  bool retrieved(unsigned int page_table_id) 
  {
    return table[page_table_id].isRetrieved();
  }
  
  void print()
  {
    std::cout << "==================================================" << std::endl;
    for(auto it = table.begin(); it != table.end();it++)
    {
      int i = it->first;
      int j = it->second.get_frame_number();
      int k = it->second.get_page_number();
      bool valid = it->second.isValid();
      bool retrieved = it->second.isRetrieved();
      std::cout << "Page: " << i << " Frame: " << j << 
      " Page: " << k << " Valid: " << valid << " Retrieved: " << retrieved << std::endl;
    }
    std::cout << "==================================================" << std::endl;
  }
};                            