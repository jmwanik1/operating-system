#pragma once

struct Entry 
{
  void set_frame_number(unsigned int i) { frame_number = i; }
  unsigned int get_frame_number() { return frame_number;  }
  
  void set_valid(bool v) { valid = v; }
  bool isValid() { return valid; }
  
  void set_page_number(unsigned int i) { page_number = i; }
  unsigned int get_page_number() { return page_number; }
  
  void set_retrieved(bool r) { retrieved = r; }
  bool isRetrieved() { return retrieved; }
  
  private:
    unsigned int frame_number = 9999;
    unsigned int page_number = 9999;
    bool valid = false;
    bool retrieved = false;
};