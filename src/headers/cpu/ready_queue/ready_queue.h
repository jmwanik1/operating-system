#pragma once

#include <cassert>
#include <deque>          // std::queue
#include <map>          // std::queue
#include <algorithm>    // std::sort
#include "./headers/cpu/pcb/pcb.h"
#include "./headers/helpers/conversions.h"

using namespace Conversions;

struct ReadyQueue 
{ 
  /**
   * Insert a pcb into the ready queue
   * 
   * @param a process control block to be added to the ready queue
  */
  void enqueue(PCB &pcb)
  {
    unsigned int job_id = hex_to_decimal(pcb.job_id);
    job_list.insert(std::make_pair(job_id, true));
    ready_queue.push_back(pcb);
  }
  
  /**
   * Retrieves a process control block(PCB) from the ready queue 
   * 
   * @param pcb_number an integer denoting the program pcb to retrieve
   * @return a process control block
  */
  PCB &dequeue() 
  { 
    assert(pcb_number < size());
    current = ready_queue.front();
    ready_queue.pop_front();
    unsigned int job_id = hex_to_decimal(current.job_id);
    job_list[job_id] = false; // make job as unavailable
    return current;
  }
  
  /**
   * Sorts the process control blocks(PCBs), where the highest priority PCBs
   * appear first. The priority is based on the priority number residing in each
   * PCB.
   * NOTE: depending on the data structure used, this might not be necessary
   * but seriously everything can be done by a vector and we just add methods
   * to manipulate the access and ordering 
   */ 
  void priority_sort() 
  { 
    std::sort (ready_queue.begin(), ready_queue.end());
  }
  
  void shortest_job_sort ()
  {
    std::sort (ready_queue.begin(), ready_queue.end(), [](const PCB& a, const PCB& b) 
    {
      int a1 = Conversions::hex_to_decimal(a.code_size);
      int b2 = Conversions::hex_to_decimal(b.code_size);
       return a1 < b2;
    });
  }
  
  /**
   * @return the current size of the ready queue
   */ 
  unsigned int size() { return ready_queue.size(); }
  
  /**
   * Clear the ready queue
   */
  void clear() {  std::deque<PCB>().swap(ready_queue); }
  
  bool empty() { return ready_queue.empty(); }
  
  void print(std::vector<PCB>& pcbs)
  {
    for(auto it = pcbs.begin(); it != pcbs.end(); it++)
      it->print();
  }
  
  bool job_exists(unsigned int job_id)
  {
    assert(job_id < max_capacity);
    return job_list[job_id];
  }
  
  private:
    std::deque<PCB> ready_queue;
    std::map<int, bool> job_list;
    unsigned int max_capacity = 30;
    PCB current;  
};