#pragma once

#include <vector>
#include <string>
#include <cassert>
#include <map>
#include <stack>
#include <queue>
#include <iostream>
#include <fstream>
#include <math.h>       /* ceil */
#include <mutex>          // std::mutex
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include "./headers/memory/memory.h"
#include "./headers/cpu/pcb/pcb.h"
#include "./headers/cpu/registers.h"
#include "./headers/helpers/conversions.h"
#include "./headers/helpers/decoder.h"
#include "./headers/memory/storage.h" 

using namespace Conversions;

class CPU {
  
  public:
    CPU(Memory &memory, std::mutex& mtx, 
        std::string& accumulator, 
        std::string& job_id, 
        std::queue<PCB*>& io_queue,
        std::vector<PCB>& page_fault_queue,
        std::vector<std::string>& finished_jobs_queue);
    ~CPU ();
    
    void init_instructions(); 
    
    Memory &memory; 
    
    void run(PCB &pcb);
    void set_ram_addresses(int start, int end);
    
    inline unsigned int get_program_counter() { return program_counter; }
    inline PCB &get_running() { return *running; }
    inline void set_program_counter(unsigned int pc) { program_counter = pc; }
    inline bool executing() { return running != NULL; }
    inline void set_busy() { busy = true; }
    inline void not_busy() { busy = false; }
    inline bool isBusy() { return busy; }
    
    // Context switch
    void dispatch();
    void save_state();
    void restore_state();
    // Job history tools
    void register_job(unsigned int i) { running_stack.push(i); }
    int previous_job() { return running_stack.top(); }
  private:
    PCB *running = NULL;
    unsigned int instructions_size = 0;
    // history
    std::stack<int> running_stack;
    // cache
    void load_cache();
    // frames and faults 
    unsigned int initial_frames = 4;
    unsigned int total_frames_read = 0;
    unsigned int page_number = 0;
    void service_page_faults(unsigned int missing_frame);
    inline void reset_page_number() { page_number = 0; };
    inline void reset_program_counter() { program_counter = 0; };
    inline void reset_initial_frames() { initial_frames = 4; };
    // locking and output
    std::mutex* mtx = nullptr;  
    std::string* accumulator = nullptr;
    std::string& job_id;
    // io queue
    std::queue<PCB*>& io_queue;
    void add_running_to_io_queue();
    // page fault queue
    std::vector<PCB>& page_fault_queue;
    void add_running_to_page_fault_queue(unsigned int, unsigned int);
    // finished jobs queue
    std::vector<std::string>& finished_jobs_queue;
    // status 
    bool busy = false;
    
    // time
    clock_t t;
    
    // cache info
    unsigned int start_in_cache = 0;
    unsigned int end_in_cache;
    unsigned int current_address;
    unsigned int inp_buff_start;
    unsigned int out_buff_start;
    unsigned int temp_buff_start;
    void init_addresses();
    
    // log file
    std::ofstream logfile;
    std::string log_filename;
    void print_log(std::string filename, std::string content);
    void print_registers();
    
    unsigned int program_counter = 0;
    unsigned int start_address_in_ram;
    unsigned int end_address_in_ram;
    
    // Compute only
    void fetch();
    void decode();
    void execute();
    void terminate(); 
    void interrupt();
    inline void set_running(PCB &pcb) { running =  &pcb; }
    
    //DMA 
    int direct_effective_address ();
    int indirect_effective_address ();
  
    // Decode helpers
    std::string instruction_as_binary();
    std::string action_to_perform(std::string &binary_instruction);
    
    // IFM = Instructions Function Map
    typedef void (CPU::*IFM)(void);
    std::map<std::string, IFM> instuct_map;
    void Call(std::string& instruct);
    
    // Execution Instructions 
    void rd();      //Reads content of I/P buffer into a accumulator
    void wr();      //Writes the content of accumulator into O/P buffer
    void st();      //Stores content of a reg. into an address
    void lw();      //Loads the content of an address into a reg.
    void mov();     //Transfers the content of one register into another
    void add();     //Adds content of two S-regs into D-reg
    void sub();     //Subtracts content of two S-regs into D-reg
    void mul();     //Multiplies content of two S-regs into D-reg
    void div_op();  //Divides content of two S-regs into D-reg
    void and_op();  //Logical AND of two S-regs into D-reg
    void or_op();   //Logical OR of two S-regs into D-reg
    void movi();    //Transfers address/data directly into a register
    void addi();    //Adds a data value directly to the content of a register
    void muli();    //Multiplies a data value directly with the content of a register
    void divi();    //Divides a data directly to the content of a register
    void ldi();     //Loads a data/address directly to the content of a register
    void slt();     //Sets the D-reg to 1 if first S-reg is less than the B-reg; 0 otherwise
    void slti();    //Sets the D-reg to 1 if first S-reg is less than a data; 0 otherwise
    void hlt();     //Logical end of program
    void nop();     //Does nothing and moves to next instruction
    void jmp();     //Jumps to a specified location
    void beq();     //Branches to an address when content of B-reg = D-reg
    void bne();     //Branches to an address when content of B-reg <> D-reg
    void bez();     //Branches to an address when content of B-reg = 0
    void bnz();     //Branches to an address when content of B-reg <> 0
    void bgz();     //Branches to an address when content of B-reg > 0
    void blz();     //Branches to an address when content of B-reg < 0
};