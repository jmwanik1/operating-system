#pragma once
#include <map>
// accumulators, index, general
#include "./headers/helpers/conversions.h"

using namespace Conversions;

struct Registers
{
  std::string instruction = "~"; // Holds the current executing instruction
  std::map<std::string, std::string> reg;
  std::string first_source;  
  std::string second_source; 
  std::string destination;   
  std::string base = "0000";          
  // Others
  std::string opcode;        
  std::string address;       
  std::string in_out_r1;     
  std::string in_out_r2;   
  
  Registers()
  {
    init();
  }
  
  Registers& operator=(Registers other)
  {
    std::swap(instruction, other.instruction);
    std::swap(reg, other.reg);
    std::swap(first_source, other.first_source);
    std::swap(second_source, other.second_source);
    std::swap(destination, other.destination);
    std::swap(opcode, other.opcode);
    std::swap(address, other.address);
    std::swap(in_out_r1, other.in_out_r1);
    std::swap(in_out_r2, other.in_out_r2);
    return *this;
  }
  
  void print_registers()
  {
    std::cout << "====== Registers =========" << std::endl;
    // trying to convert it.second with size > x 
    // where x is some big num idk what causes an out of range error
    for(auto it: reg)
    {
      if(it.second != "")
      {
        std::cout <<  binary_to_decimal(it.first) << "\t" <<
                      it.first << "\t" << 
                      binary_to_decimal(it.second) << std::endl;
      }
      else
      {
        std::cout <<  binary_to_decimal(it.first) << "\t" <<
                      it.first << "\t" << "X" << std::endl;
      } 
    }
    std::cout << "=============================" << std::endl;
  }
  
  void insert_registers(std::map<std::string, std::string> reg)
  {
    for (auto r: reg)
    {
      reg.insert(r);
    }
  }
  
  
  private:
    //initializes the 16 registers
    void init()
    {
      reg.insert(std::make_pair("0000", "0000")); // 0 - accumulator 
      reg.insert(std::make_pair("0001", "0000")); // 1 - zero register
      reg.insert(std::make_pair("0010", "")); // 2
      reg.insert(std::make_pair("0011", "")); // 3
      reg.insert(std::make_pair("0100", "")); // 4
      reg.insert(std::make_pair("0101", "")); // 5
      reg.insert(std::make_pair("0110", "")); // 6
      reg.insert(std::make_pair("0111", "")); // 7
      reg.insert(std::make_pair("1000", "")); // 8
      reg.insert(std::make_pair("1001", "")); // 9
      reg.insert(std::make_pair("1010", "")); // 10
      reg.insert(std::make_pair("1011", "")); // 11
      reg.insert(std::make_pair("1100", "")); // 12
      reg.insert(std::make_pair("1101", "")); // 13
      reg.insert(std::make_pair("1110", "")); // 14
      reg.insert(std::make_pair("1111", "")); // 15
    }
};
