#pragma once

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdlib.h>     /* strtoull */
#include <bitset>

namespace Conversions 
{
  /**
   * Convert a hex string to decimal
   * 
   * @param hex a string representing a hex value
   */ 
  int hex_to_decimal(std::string hex);
  
  /**
   * Convert a hex string to binary
   * 
   * @param hex a string representing a hex value
   */ 
  std::string hex_to_binary(std::string hex);
  
  /**
   * Convert a decimal to binary
   * 
   * @param num an integer
   * @return a string representing the integer binary equivalence
   */ 
  std::string decimal_to_hex(int decimal); 
  
  std::string decimal_to_binary(unsigned int num);
  
  unsigned long long int binary_to_decimal(std::string bin);


  std::string binary_to_hex(std::string bin);
};
