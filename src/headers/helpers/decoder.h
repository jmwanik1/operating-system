#pragma once
#include <iostream>
#include <string>
#include <cassert>

#include "./headers/cpu/registers.h"

namespace Decoder
{
  void arithmetic(Registers &reg, std::string &bin, unsigned int start);
  void conditional(Registers &reg, std::string &bin, unsigned int start);
  void unconditional(Registers &reg, std::string &bin, unsigned int start);
  void io(Registers &reg, std::string &bin, unsigned int start);
  std::string retrieve_bits(std::string &bin, unsigned int, unsigned int);
};
