#pragma once

#include <iostream>

namespace Logger
{
  void log(std::string message);
};
