#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include "./headers/cpu/dispatcher/dispatcher.h"
#include "./headers/memory/memory.h"
#include "./headers/schedulers/long_term_scheduler.h"
#include "./headers/schedulers/short_term_scheduler.h"
#include "./headers/helpers/logger.h"

class Schedulers
{
    
  private:
    Dispatcher *dispatcher;
    Memory& memory; 
    unsigned int num_of_programs = 27;
  public:
    Schedulers(Memory& memory, Dispatcher &dispatcher);
    LongTermScheduler long_term_scheduler;
    ShortTermScheduler short_term_scheduler;
    void schedule();
    void init();
};