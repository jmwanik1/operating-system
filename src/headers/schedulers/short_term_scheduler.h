#pragma once

#include <queue>
#include <iostream>
#include <string>
#include <fstream>
#include "./headers/memory/memory.h"
#include "./headers/cpu/dispatcher/dispatcher.h"
#include "./headers/helpers/logger.h"
#include "./headers/helpers/conversions.h"

using namespace Conversions;

class ShortTermScheduler 
{
  private:
    Dispatcher &dispatcher;
    void dispatch();
    void service_queues();
    void print_output();
  public:
    ShortTermScheduler(Dispatcher &disp);
    void init();
    void schedule();
    void FCFS();
    void PRIORITY();
    void SJF();
};