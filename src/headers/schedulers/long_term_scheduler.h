#pragma once

#include <queue>
#include <iostream>
#include <string>
#include "./headers/memory/memory.h"
#include "./headers/cpu/pcb/pcb.h"
#include "./headers/helpers/logger.h" // for logging
#include "./headers/cpu/ready_queue/ready_queue.h"

class LongTermScheduler 
{
  private:
    unsigned int consumed_pcbs = 0;
    unsigned int consumed_pages = 0;
    unsigned int batch_limit = 15;
    unsigned int initial_frames_limit = 4;
    unsigned int frame_number = 0;
    ReadyQueue ready_queue;
    Memory& memory; 
    
    /**
     * Transfer program disk content to ram and update the respective program PCB
     * values with respect to where they reside in ram
     */ 
    void disk_to_ram();
    
    /**
     * Add the given integer to the pcb, in this case, the start address in ram
     * 
     * params pcb a process control block object 
     * params start_address_in_ram an integer denoting the start value of the 
     * process instructions in ram
     */ 
    void add_to_pcb(PCB &pcb, int start_address_in_ram);
    
    /**
     * Set the number of consumed pcbs, this number happens to be the 
     * current size of the ready queue
     * 
     * params i an integer value to set as the number of consumed pcbs
     */ 
    inline void increase_consumed_pcbs(unsigned int i) { consumed_pcbs += i; }
    inline void set_batch_limit(unsigned int i) { batch_limit = i; }
    inline void set_consumed_pcbs(unsigned int i) { consumed_pcbs = i; }
    inline bool even(int i) { return (i % 2 == 0); }
    inline bool odd(int i) { return !even(i); }
    void old_disk_to_ram_transfer();
    void update_pcb_info();
    void update_page_table(PCB& current, int);
    void pages_to_ram(PCB& current, unsigned int limit);
  public:
    LongTermScheduler(Memory& memory);
    void init();
    void schedule();
    inline unsigned int get_consumed_pcbs() { return consumed_pcbs; }
};