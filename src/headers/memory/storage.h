#pragma once

#include <iostream>
#include <fstream>
#include <stdlib.h>     
#include <time.h> 
#include <cassert>
#include <map>
#include <vector>
#include <algorithm>    // std::sort
#include "./headers/helpers/conversions.h"

struct Storage 
{ 
  
  std::vector<std::string> content; 
  std::vector<std::vector<std::string>> store;
  std::vector<int> free_frames;
  
  
  Storage& operator=(Storage other)
  {
    std::swap(content, other.content);
    std::swap(store, other.store);
    std::swap(free_frames, other.free_frames);
    return *this;
  }
  
  void allocate_storage(unsigned int s)
  {
    for (unsigned int i = 0; i < s; i++)
      content.push_back("0");
  }
  
  // NOTE: private members defined at the bottom
  unsigned int max_size;
  
  /**
   * Set the max capacity of storage device
   */
  void set_capacity(unsigned int i)
  {
    max_size = i;
  }
   
  /**
   * Max capacity, where each word is 4 instructions
   */
   unsigned int max_capacity() { return max_size / 4; }
  
  /**
   * Insert a string into the storage space 
   * 
   * @param a string containing the item to store in the storage
  */
  void insert(const std::string &input)
  {
    content.push_back(input);
  }
  
  /**
   * Add a vector to hold content of a word
   */ 
  void new_word()
  {
    std::vector<std::string> w;
    store.push_back(w);
  }
  
  /**
   * Initialize storage with empty frames
   */ 
  void init_empty_frames()
  {
    for (unsigned int i = store_size(); i < max_capacity(); i++)
      new_word();
  }
  
  /**
   * Insert the given instruction at the specified word location
   */ 
  void insert(const std::string in, unsigned int i)
  {
    assert(i < store.size());
    if (i < store.size())
      store.at(i).push_back(in);
    else
      std::cerr << "ERROR: out of range in insert(string, int)" << std::endl;       
  }
  
  void store_insert(const std::string in)
  {
    store.back().push_back(in);
  }
  
  /**
   * Store the given vector at the specified location in storage
   * @param vector the vector to store 
   * @param int location to store the vector
   */
  void store_insert(const std::vector<std::string>& frame, unsigned int frame_number)
  {
    assert(frame_number < store.size());
    store[frame_number] = frame;
  }  
  /**
   * Insert the vector to the storage store
   */
  void store_insert(const std::vector<std::string>& input)
  {
    store.push_back(input);
  }
  
  /**
   * Write the given data into the storage
   * @param int page_number denoting the page to write content to 
   * @param offset int the offset into the page where to store the data
   * @param data string the content to store in the storage
   */ 
  void store_write(unsigned int page_number, unsigned int offset, std::string data)
  {
    assert(offset < 4);
    assert(page_number < store.size());
    store.at(page_number)[offset] = data;
  }
  /**
   * Insert the content of a vector into the storage space 
   * 
   * @param input a vecotr containing the items to store in the storage
  */
  void insert(const std::vector<std::string>& input)
  {
    for(auto item : input)
      insert(item);
  }
  
  void insert(const std::vector<std::string>& input, unsigned int start)
  {
    std::cout << "Storage size: " << size() << std::endl;
    for(auto item : input)
    {
      content.insert(content.begin() + start, item);
      start += 1;
    }
  }
  
  /**
   * Retreives pages or memory frames from the 
   * storage device as a word: disk or ram. In this case a 
   * word is 4 instructions
   */
  std::vector<std::string> word(int start) 
  { 
    unsigned int end = 4;
    // TODO: make sure start and end are within scope
    std::vector<std::string> block;
  
    for (unsigned int i = start; i < start + end; i++)
      if (i < content.size())
        block.push_back(content.at(i));
  
    return block;
  }
  
  /**
   * Retrieves data from storage as a block. 
   * 
   * @param start start index of the storage block
   * @param end index where the storage block ends
   * @return a vector containing specified storage block
  */
  std::vector<std::string> fetch(int start, int end) 
  { 
    // TODO: make sure start and end are within scope
    std::vector<std::string> block;
  
    for (int i = start; i <= end; i++)
      block.push_back(content.at(i));
  
    return block;
  }
  
  /**
   * Retrieve a word from storage
   */ 
   std::vector<std::string>& retrieve(int i)
   {
    //lock.lock();
     assert(i < store.size());
     return store.at(i);
     //lock.unlock();
   }
   
  std::string fetch(int location) //Do you use this one?
  {
    assert(location < content.size());
    return content.at(location);
  }

  /** 
   * Boolean method for checking if Storage is full. 
   * 
   * @return true if storage is not full, false otherwise 
  */
  bool full() { return size() == max_size; }
  
  unsigned int size() { return content.size(); }
  
  unsigned int store_size() { return store.size(); }
  
  bool space_available(unsigned int space_size) 
  {
    return ((size() + space_size) <= max_size + 1);
  }
  
  /**
   * Write the given data content to the specified memory location
   * 
   * @param location valid integer index of the storage 
   * @param data string to be written(inserted) at the specified location
   */ 
  void write(unsigned int location, std::string& data)
  {
    //lock.lock();
    // Note: Make sure location is in bounds
    assert(location < content.size());
    content.at(location) = data;
    //lock.unlock();
  }
  
  void insert_free_frame(int frame) { free_frames.push_back(frame); }
  
  void free_frame(int frame) 
  { 
    free_frames.erase(std::remove(free_frames.begin(), 
      free_frames.end(), frame), free_frames.end());
  }
  
  int free_frames_size() { return free_frames.size(); }
  
  //-1 means no frame found
  int get_free_frame() 
  { 
    std::cout << "Getting free frame" << std::endl;
    int frame = free_frames.back();
    std::cout << "Frame list size: " << free_frames_size() << std::endl;
    return frame;
  }
  
  /**
   * Clear storage
   */
  void clear() {  std::vector<std::string>().swap(content); }
  
  /**
   * Clear store
   */
  void clear_store() {  std::vector<std::vector<std::string>>().swap(store); }
  
  // print storage content
  void print() 
  {
    std::ofstream logfile;
    logfile.open("storage_content.txt");
    for (auto it = content.begin(); it != content.end(); ++it)
    {
      logfile << *it << std::endl;
    }
    logfile.close();
  }
  
  void print(std::string output_file_name, unsigned int job_id)
  {
    print(0, content.size(), output_file_name, job_id);
  }
  
  void print(int start, int end, std::string output_file_name, int id) 
  {
    std::ofstream logfile;
    logfile.open(output_file_name);
    logfile << "Process: " << id << std::endl;
    int c = start;
    for (auto it = content.begin(); (c != end && it != content.end()); ++it)
    {
      logfile << c << "\t\t" <<  *it << std::endl;
      c += 1;
  
    }
    logfile.close();
  }
  
  void store_print() 
  {
    std::ofstream logfile;
    logfile.open ("storage_store.txt");
    logfile << "Size: " << store.size() << std::endl;
    int c = 0;
    for (auto it = store.begin(); it != store.end(); ++it)
    {
      logfile << "=========" << c << "==========" << std::endl;
      for (auto item : *it)
      {
        logfile << item << std::endl;
      }
      logfile << "=====================" << std::endl;
      c += 1;
    }
    logfile.close();
  }
  
  void print_free_frame_list()
  {
    std::sort(free_frames.begin(), free_frames.end());
    std::ofstream logfile;
    logfile.open ("free_frames_list.txt");
    logfile << "Size: " << free_frames.size() << std::endl;
    int c = 0;
    for (auto it = free_frames.begin(); it != free_frames.end(); ++it)
    {
      logfile << "=========" << c << "==========" << std::endl;
      logfile << *it << std::endl;
      logfile << "=====================" << std::endl;
      c += 1;
    }
    logfile.close();
  }
};