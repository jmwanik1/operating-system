#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include "./headers/memory/loader.h"
#include "./headers/memory/storage.h" 
#include "./headers/helpers/logger.h"

class MemoryManager
{
  private:
    Loader &loader;
    const unsigned int MAX_DISK_SIZE = 2048;
    const unsigned int MAX_RAM_SIZE = 1024;
    void init();
    
  public:
    Storage disk;
    Storage ram;
    MemoryManager(Loader &loader);
    void init_free_frames_in_ram();
};