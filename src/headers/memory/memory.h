#pragma once

#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <queue>
#include <vector>
#include "./headers/memory/memory_manager.h"
#include "./headers/memory/loader.h"
#include "./headers/cpu/pcb/pcb.h"
#include "./headers/helpers/logger.h"
#include "./headers/cpu/ready_queue/ready_queue.h"


class Memory {
  private:
    const char *file = NULL;
    std::vector<ReadyQueue> queues;
    /**
     * Extract the // Job and // Data cards from the program content 
     * and invoke insert operations on each to put them into the PCB
     */
    void extract_process_cards();
    
    /**
     * Split the given job info string and insert it into the pcb
     * 
     * @param job_info a string containing the job card information from 
     * the loaded program file
     */
    void insert_job_card_into_pcb(std::string job_info, int start);
    
    /**
     * Insert the data info into the pcb, NOTE: the data info is constant,
     * as in // Data 14 C C for all programs in the program file
     */
    void insert_data_card_into_pcb();


    void insert_end_of_program_card_into_pcb(unsigned int value);

    /**
     * After the Job card has been created, this method is invoked to create the
     * PCB and insert it in the process control blocks vector
     * 
     * @param pcb a process control block representing the created pcb from 
     * values retrieved from the loaded program file
     */ 
    void create_pcb(PCB pcb);

    /**
     * Insert the instructions and data of each program in the program file 
     * in the disk 
     */ 
    void insert_program_file_into_disk();
    
    inline bool even(int i) { return (i % 2 == 0); }
    inline bool odd(int i) { return !even(i); }
    
    void update_pcb_info();
  public:
    Loader loader;
    MemoryManager memory_manager; 
    std::vector<PCB> process_control_blocks;
    std::vector<std::string> programs;
    std::vector<int> program_sizes;
    unsigned int memory_size = 0;
    void print();
    std::string fetch(int location, PCB* running);
    void write(int location, std::string& data);
    
    Memory(const char *file_name);    
    
    /**
     * Extract process cards and create PCB for each program from the loaded 
     * program file and then save the program file to disk.
     */ 
    void load();
    
    void print_pcbs();
    
    void new_queue();
    
    inline void add_queue(ReadyQueue queue) { queues.emplace_back(queue); }
    
    inline bool queues_empty() { return queues.empty(); }
    
    inline int num_of_queues() { return queues.size(); }
    
    inline ReadyQueue& ready_queue() { return queues.front(); }
    
    inline ReadyQueue& last_queue() { return queues.back(); }
    
    void remove_queue();
};