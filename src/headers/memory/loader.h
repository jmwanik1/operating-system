#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <algorithm>

class Loader {
  private:
    const char *file = NULL;
    std::vector<std::string> prgms;
    /**
    	Initializes the Loader object
    */
    void init();
    
  public:
    /**
  		The Loader object reads the program file and stores the content
  		in disk. 
  		
  		@param file_name name of the program file
  		@return an empty loader object
  	*/
  	Loader(const char *file_name);
  	
    /**
    	Returns a vector reference of the input file(program file) content
    */
    std::vector<std::string> &programs();
    
    std::vector<std::string> subset(unsigned int start, unsigned int n);
    
    /**
    	Reads the input file(program file), skipping any whitespace 
    	and stores the content in a vector object.
    	
    	@return true(0) if file successfully read, false(-1) otherwise
    */
    bool read();
    
    /**
  	  * Trims the instructions of '0x' in the vector prgms.
  	  * 
  	 */
    void trim();
    
    
    // print
    void print();
};