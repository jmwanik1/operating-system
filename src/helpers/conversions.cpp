#include "./headers/helpers/conversions.h"

namespace Conversions 
{
  int hex_to_decimal(std::string hex)
  {
    std::stringstream ss;
    ss.str(hex);
    int decimal;
    ss >> std::hex >> decimal;
    return decimal;
  }
  
  std::string hex_to_binary(std::string hex)
  {
    std::string binary;
    for(char &c : hex)
    {
      switch(std::toupper(c))
      {
        case '0': binary.append ("0000"); break;
				case '1': binary.append ("0001"); break;
				case '2': binary.append ("0010"); break;
				case '3': binary.append ("0011"); break;
				case '4': binary.append ("0100"); break;
				case '5': binary.append ("0101"); break;
				case '6': binary.append ("0110"); break;
				case '7': binary.append ("0111"); break;
				case '8': binary.append ("1000"); break;
				case '9': binary.append ("1001"); break;
				case 'A': binary.append ("1010"); break;
				case 'B': binary.append ("1011"); break;
				case 'C': binary.append ("1100"); break;
				case 'D': binary.append ("1101"); break;
				case 'E': binary.append ("1110"); break;
				case 'F': binary.append ("1111"); break;
      }
    }
    return binary;
  } 

  std::string decimal_to_binary(unsigned int num)
  {
    std::string binary = std::bitset<8>(num).to_string(); //to binary
    return binary;
  }
  
  std::string decimal_to_hex(int decimal) 
  {
    std::stringstream ss;
    ss << std::hex << decimal; 
    std::string result (ss.str());
    return result;
  }
  
  unsigned long long int  binary_to_decimal(std::string bin)
  { 
    if(bin == "")
      bin = "0000";
    std::bitset<32> bits(bin);
    return bits.to_ulong();
  }
  
  
  std::string binary_to_hex(std::string bin)
  {
    unsigned long long int  res = binary_to_decimal(bin);
    return decimal_to_hex(res);
  }
}