#include "./headers/helpers/decoder.h"

namespace Decoder 
{
  
  std::string retrieve_bits(std::string &bin, unsigned start, unsigned end)
  {
    assert(bin.length() > (end - start));
    assert(bin.length() > 0);
    std::string result;
    
    //Loop retrieves bits and adds them to result string
    if (bin.length() > 0)
    {
      for(unsigned int i = start; i < end; i++)
        if (i < bin.size())
          result += bin.at(i);
    }
    return result;
  }
  
  void arithmetic(Registers &reg, std::string &bin, unsigned int start)
  {
    //retrieves the bits for the first S-reg
    reg.first_source = retrieve_bits(bin, start, start + 4); 
    start += 4;
    //retrieves the bits for the second S-reg
    reg.second_source = retrieve_bits(bin, start, start + 4);
    start += 4; 
    //retrieves the bits for the D-reg
    reg.destination = retrieve_bits(bin, start, start +  4);
    start += 4; 
    
    reg.address = retrieve_bits(bin, start, start + 12);
  }
  
  void conditional(Registers &reg, std::string &bin, unsigned int start)
  {
    unsigned int max = 32;
    //retrieves the bits for the B-reg
    reg.base = retrieve_bits(bin, start, start + 4); 
    start += 4; 
    //retrieves the bits for the D-reg
    reg.destination = retrieve_bits(bin, start, start + 4); 
    start += 4;  
    //retrieves the bits for the address
    reg.address = retrieve_bits(bin, start, max);
  }
  
  void unconditional(Registers &reg, std::string &bin, unsigned int start)
  {
    //retrieves the bits for the address
    reg.address = retrieve_bits(bin, start, 32);
  }
  
  void io(Registers &reg, std::string &bin, unsigned int start)
  {
    unsigned int max = 32;
    //retrieves the bits for Reg-1
    reg.in_out_r1 = retrieve_bits(bin, start, start + 4);
    start += 4;
    //retrieves the bits for Reg-2
    reg.in_out_r2 = retrieve_bits(bin, start, start + 4);
    start += 4;
    //retrieves the bits for the address
    reg.address = retrieve_bits(bin, start, max);
  }
}