#include "./headers/helpers/logger.h"

namespace Logger 
{
  void log(std::string message)
  {
      std::cout << message << std::endl;
  }
}