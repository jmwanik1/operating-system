#pragma once

#include <string>


struct Page {
  public:
  Page() 
  {   
    static int pageNumber = 0; _id = id++;
  }
  void insert (string arr [])
  {
    for (int i = 0; i<=4; i++)
    {
      content [i] = arr [i];
    }
  }
  
  bool in_RAM ()
  {
    return inRAM;
  }
  
  private:
  bool inRAM = false;
  int pageNumber; //Should be unique from constructor.
  string content [4]; //Instead of this we could just have a start and end address
  //of it in ram. Not sure how we want to impleament.
  //Ex.)
  //int start in RAM;
  //int end IN ram ; <--- Would also neeed them in disk.
};
