#include "./headers/schedulers/long_term_scheduler.h"

LongTermScheduler::LongTermScheduler(Memory& memory) : memory(memory)
{
  Logger::log("Long term scheduler created...");
}


void LongTermScheduler::init()
{
  Logger::log("==============================================================");
  Logger::log("Initializing long term scheduler...");
  schedule();
  Logger::log("Long term scheduler initialized...");
  Logger::log("==============================================================");
}

void LongTermScheduler::schedule()
{
  Logger::log("Long term scheduler scheduling...");
  ready_queue.clear();
  memory.memory_manager.ram.clear();
  memory.memory_manager.ram.clear_store();
  disk_to_ram();
  memory.add_queue(ready_queue);
  Logger::log("Long term scheduler finished scheduling...");
}

void LongTermScheduler::disk_to_ram()
{
  Logger::log("Transfering pages from disk to ram as frames...");
  // Get the pages from disk to ram based on the information in pcb
  unsigned int pcbs_size = memory.process_control_blocks.size();
  for (unsigned int i = 0; i < pcbs_size; i++)
  {
    PCB &current = memory.process_control_blocks.at(i);
    pages_to_ram(current, initial_frames_limit);
    // change state of pcb(process) and add it to ready queue
    current.state.set_ready();
    ready_queue.enqueue(current);
  }
  memory.memory_manager.ram.init_empty_frames();
  Logger::log("Completed pages transfer to ram(frames)...");
} 

void LongTermScheduler::pages_to_ram(PCB& current, unsigned int limit)
{
  unsigned int start = current.start_page_in_disk;
  unsigned int start_frame_in_ram = memory.memory_manager.ram.store_size();
  current.start_frame_in_ram = start_frame_in_ram;
  int pages_accounted = 0; 
  for (unsigned i = start; i < start + limit; i++)
  {
    std::vector<std::string> word = memory.memory_manager.disk.retrieve(i);
    memory.memory_manager.ram.store_insert(word);
    int account_page_in_disk = current.start_page_in_disk + pages_accounted;
    current.page_table.insert
      (current.frames_assigned, frame_number, account_page_in_disk, true);
    // remove frame from list of free frames
    memory.memory_manager.ram.free_frame(frame_number);
    current.frames_assigned += 1;
    // frames assigned so far
    frame_number += 1;
    // account the page location in disk
    pages_accounted += 1;
  }
  update_page_table(current, pages_accounted);
}

void LongTermScheduler::add_to_pcb(PCB &pcb, int start_address_in_ram) 
{
  if(start_address_in_ram < 0)
    start_address_in_ram = 0;
  pcb.start_address_in_ram = start_address_in_ram;
} 

void LongTermScheduler::update_page_table(PCB &current, int pages_accounted)
{
  for(unsigned int i = current.frames_assigned; i < current.total_pages; i++)
  {
    int account_page_in_disk = current.start_page_in_disk + pages_accounted;
    current.page_table.insert(i, 0, account_page_in_disk, false);
    pages_accounted += 1;
  }
}