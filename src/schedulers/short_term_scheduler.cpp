#include "./headers/schedulers/short_term_scheduler.h"

ShortTermScheduler::ShortTermScheduler(Dispatcher &disp) : dispatcher(disp)
{
  Logger::log("Short term scheduler created...");
}

void ShortTermScheduler::init()
{
  Logger::log("Initializing short term scheduler...");
  Logger::log("Short term scheduler initialized...");
}

void ShortTermScheduler::dispatch()
{
  dispatcher.dispatch();
}

void ShortTermScheduler::FCFS()
{
  while (!dispatcher.memory.ready_queue().empty())
  {
    dispatcher.dispatch();
  }
  
  dispatcher.memory.remove_queue();
  if (!dispatcher.page_fault_queue.empty())
    { // Page fault servicer
      std::vector<PCB> unsuccessful;
      
      dispatcher.memory.new_queue();
      int c = 0;
      auto it = dispatcher.page_fault_queue.begin();
      for (; it != dispatcher.page_fault_queue.end(); it++)
      {
        
        PCB& pcb = *it;
        std::cout << "Job id: " << hex_to_decimal(pcb.job_id) << std::endl;
        if(!dispatcher.page_fault_handler(&pcb))
          unsuccessful.push_back(pcb);
        c += 1;
      }
      dispatcher.clear_page_fault_queue();
      dispatcher.insert_to_page_fault_queue(unsuccessful);
      std::cout << "Serviced Jobs: " << c << std::endl;
      std::cout << "Page fault queue size: " << dispatcher.page_fault_queue.empty() << std::endl;
      std::cout << "Servicing completed..." << std::endl;
      std::cout << "Num of queues: " << dispatcher.memory.num_of_queues() << std::endl;
      FCFS();
    }     
  print_output();
}

void ShortTermScheduler::PRIORITY()
{
  dispatcher.memory.ready_queue().priority_sort();
  while (!dispatcher.memory.ready_queue().empty())
  {
    dispatcher.dispatch();
  }
  
  dispatcher.memory.remove_queue();
  if (!dispatcher.page_fault_queue.empty())
  { // Page fault servicer
    std::vector<PCB> unsuccessful;
    
    dispatcher.memory.new_queue();
    int c = 0;
    auto it = dispatcher.page_fault_queue.begin();
    for (; it != dispatcher.page_fault_queue.end(); it++)
    {
      
      PCB& pcb = *it;
      std::cout << "Job id: " << hex_to_decimal(pcb.job_id) << std::endl;
      if(!dispatcher.page_fault_handler(&pcb))
        unsuccessful.push_back(pcb);
      c += 1;
    }
    dispatcher.clear_page_fault_queue();
    dispatcher.insert_to_page_fault_queue(unsuccessful);
    std::cout << "Serviced Jobs: " << c << std::endl;
    std::cout << "Page fault queue size: " << dispatcher.page_fault_queue.empty() << std::endl;
    std::cout << "Servicing completed..." << std::endl;
    std::cout << "Num of queues: " << dispatcher.memory.num_of_queues() << std::endl;
    PRIORITY();
  }     
  print_output();
}

void ShortTermScheduler::SJF()
{
  dispatcher.memory.ready_queue().shortest_job_sort();
  while (!dispatcher.memory.ready_queue().empty())
  {
    dispatcher.dispatch();
  }
  
  dispatcher.memory.remove_queue();
  if (!dispatcher.page_fault_queue.empty())
  { // Page fault servicer
    std::vector<PCB> unsuccessful;
    
    dispatcher.memory.new_queue();
    int c = 0;
    auto it = dispatcher.page_fault_queue.begin();
    for (; it != dispatcher.page_fault_queue.end(); it++)
    {
      
      PCB& pcb = *it;
      std::cout << "Job id: " << hex_to_decimal(pcb.job_id) << std::endl;
      if(!dispatcher.page_fault_handler(&pcb))
        unsuccessful.push_back(pcb);
      c += 1;
    }
    dispatcher.clear_page_fault_queue();
    dispatcher.insert_to_page_fault_queue(unsuccessful);
    std::cout << "Serviced Jobs: " << c << std::endl;
    std::cout << "Page fault queue size: " << dispatcher.page_fault_queue.empty() << std::endl;
    std::cout << "Servicing completed..." << std::endl;
    std::cout << "Num of queues: " << dispatcher.memory.num_of_queues() << std::endl;
    SJF();
  }     
  print_output();
}

void ShortTermScheduler::print_output()
{
  std::ofstream logfile;
  logfile.open("output.txt");
  for (unsigned int i = 0; i < dispatcher.finished_jobs_queue.size(); i++)
  {
    logfile << dispatcher.finished_jobs_queue.at(i) << std::endl;
  }
  dispatcher.memory.memory_manager.ram.print_free_frame_list();
}