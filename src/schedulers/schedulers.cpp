#include "./headers/schedulers/schedulers.h"

Schedulers::Schedulers(Memory& memory, Dispatcher& dispatcher) : 
memory(memory),
long_term_scheduler(memory),
short_term_scheduler(dispatcher)
{
  Logger::log("==============================================================");
  init();
  Logger::log("Schedulers created and initialized");
  Logger::log("==============================================================");
}

void Schedulers::init()
{
  Logger::log("Initializing schedulers...");
  long_term_scheduler.init();
  short_term_scheduler.init();
  Logger::log("Schedulers initialized...");
}

void Schedulers::schedule()
{
  short_term_scheduler.SJF();
  // long_term_scheduler.schedule();
  // short_term_scheduler.PRIORITY();
}
