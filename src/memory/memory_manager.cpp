#include "./headers/memory/memory_manager.h"

MemoryManager::MemoryManager(Loader &l) : loader(l) 
{
  init();
}

void MemoryManager::init()
{
  disk.max_size = MAX_DISK_SIZE;
  ram.max_size = MAX_RAM_SIZE;
}

void MemoryManager::init_free_frames_in_ram()
{
  Logger::log("Initializing free frames...");
  unsigned int frame_size = 255;
  // Sentinel
  ram.insert_free_frame(-1);
  for (unsigned int i = 1; i < frame_size; i++)
    ram.insert_free_frame(i);
  Logger::log("Free frames initialized...");
}

