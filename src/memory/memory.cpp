#include "./headers/memory/memory.h"

Memory::Memory(const char *file_name) : 
  loader(file_name),
  memory_manager(loader)
{
  Logger::log("Empty memory created and initialized...");
}

void Memory::load()
{
  Logger::log("Loading memory...");
  extract_process_cards();
  insert_program_file_into_disk();
  update_pcb_info();
  memory_manager.init_free_frames_in_ram();
  Logger::log("Finished loading memory...");
}

void Memory::extract_process_cards()
{
  Logger::log("Extracting program cards into PCB...");
  unsigned int counter = 0;
  for(auto it = loader.programs().begin(); it != loader.programs().end(); it++)
  {
    std::string item = *it;
    bool is_job_card_info = (item.find("// JOB") != std::string::npos);
    bool is_data_card_info = (item.find("Data") != std::string::npos);
    bool is_end_card_info = (item.find("END") != std::string::npos);
    if(is_job_card_info) 
    {
      insert_job_card_into_pcb(item, counter);   // insert job info into pcb
    }
    else if(is_data_card_info)
    {
      insert_data_card_into_pcb();     // insert data card into pcb
    }
    else if(is_end_card_info)
    {
      insert_end_of_program_card_into_pcb(counter - 1);   // insert job info into pcb
    }
    else
    {
      programs.push_back(*it);
      counter += 1;
    }
  }
  Logger::log("Finished extracting program cards into PCB...");
}

void Memory::insert_job_card_into_pcb(std::string job_info, int start)
{
  PCB pcb;
  // insert start address in disk
  pcb.start_address_in_disk = start;
  unsigned int i = 1;
  std::string info;
  std::istringstream iss(job_info, std::istringstream::in);

  while(iss >> info)
  {
    switch(i)
    {
      case 3:
        pcb.job_id = info;
      break;
      case 4:
        pcb.code_size = info;
      break;
      case 5:
        pcb.priority = info;
      break;
    }
    
    i += 1;
  }
  
  create_pcb(pcb);
}

void Memory::insert_data_card_into_pcb()
{
  process_control_blocks.back().input_buffer_size = "14";
  process_control_blocks.back().output_buffer_size = "C";
  process_control_blocks.back().temp_buffer_size = "C";
  memory_size = process_control_blocks.back().data_size();
}

void Memory::insert_end_of_program_card_into_pcb(unsigned int value)
{
  process_control_blocks.back().end_address_in_disk = value;
}

void Memory::create_pcb(PCB pcb)
{
  process_control_blocks.push_back(pcb);
  // store the size of the program for page creation
  program_sizes.push_back(pcb.instructions_size());
}

void Memory::insert_program_file_into_disk()
{
  Logger::log("Inserting program file into disk...");
  std::cout << programs.size() << std::endl;
  auto it = programs.begin();
  int k = 1;
  for(unsigned int size : program_sizes)
  {
    for(unsigned int i = 0; i < size; i++)
    {
      if (i % 4 == 0)
        memory_manager.disk.new_word();
      memory_manager.disk.insert(*it);
      memory_manager.disk.store_insert(*it);
      it++;
    }
    // Data
    for(unsigned int i = 0; i < memory_size; i++)
    {
      if (i % 4 == 0)
        memory_manager.disk.new_word();
      memory_manager.disk.insert(*it);
      memory_manager.disk.store_insert(*it);
      it++;
    }
    k += 1;
  }
  Logger::log("Finished inserting program file into disk...");
  memory_manager.disk.store_print();
}

void Memory::update_pcb_info()
{
  Logger::log("==============================================================");
  Logger::log("Update pcb about pages in disk...");
  unsigned int pcbs_size = process_control_blocks.size();
  for (unsigned int i = 0; i < pcbs_size; i++)
  {
    PCB &current = process_control_blocks.at(i);
    int inst_pages = current.instructions_size() / 4; 
    int total_pages = current.total_size() / 4;

    if (odd(current.total_size()))
      total_pages += 1;
      
    if(odd(current.instructions_size()))
      inst_pages += 1;
    
    current.total_pages = total_pages;
    current.total_instruction_pages = inst_pages;
    if (i == 0) // first process start frame is the 0th frame
    {
      current.start_page_in_disk = 0; 
      /*std::cout << "============= Process: " << i + 1 << " ===============" << std::endl;
      std::cout << "start page: " << current.start_page_in_disk << std::endl;
      std::cout << "Data_start_page: " << 0 + inst_pages << std::endl;
      std::cout << "total page: " << total_pages << std::endl;*/ 
    }
    else // let's handle the case for all other frames
    {
      PCB &prev = process_control_blocks.at(i-1);
      int current_start = prev.total_pages + prev.start_page_in_disk;
      current.start_page_in_disk = current_start; 
      current.data_start_page_in_disk = current_start + inst_pages;
      /*std::cout << "============= Process: " << i + 1 << " ===============" << std::endl;
      std::cout << "start page: " << current_start << std::endl;
      std::cout << "data start page: " << current.data_start_page_in_disk << std::endl;
      std::cout << "total pages: " << total_pages << std::endl; */
    }
  } 
  Logger::log("Completed updating pcb about pages in disk...");
  Logger::log("==============================================================");
}

void Memory::new_queue() 
{ 
  ReadyQueue queue; 
  add_queue(queue); 
}

void Memory::remove_queue() 
{ 
  assert(!queues.empty());
  queues.erase(queues.begin());
}


void Memory::print_pcbs()
{
  for(auto pcb: process_control_blocks)
  {
    pcb.print();
  }
}

void Memory::print()
{
	for(auto it = programs.begin(); 
			it != programs.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
}