#include "./headers/memory/loader.h"

Loader::Loader(const char *file_name)
{
  file = file_name; 
	init();
}

void Loader::init()
{
	read();
	trim(); 
}
    
std::vector<std::string>& Loader::programs()
{
  return prgms;
}
    
bool Loader::read()
{
	std::ifstream fin(file, std::ios::in);	
	std::string str = "";
	
	if(fin)
	{
		while(std::getline(fin, str)) 
		{
			if(!str.empty())
      	prgms.push_back(str);
		}
		
		fin.close();
		return true;
	}
	else
	{
		std::cout << "An error occurred while opening the file" << std::endl;
		std::cerr << "Error: " << strerror(errno) << std::endl;
		return false;
	}
}

std::vector<std::string> Loader::subset(unsigned int start, unsigned int n)
{
	// TODO: Make sure subset is within scope
	std::vector<std::string>::const_iterator begin = prgms.begin() + start;
	std::vector<std::string>::const_iterator end = prgms.begin() + start + n;
  std::vector<std::string> sub(begin, end);
  return sub;
}
    
void Loader::trim() 
{
  std::string s = "0x";
  for(auto it = prgms.begin(); it != prgms.end(); it++)
		{
		  std::string str = *it;
		  std::size_t found = str.find(s); 
		
		  if (found != std::string::npos) 
		  {
		    str.erase(found, s.length());
		    *it = str;
		  }
		}
  }
  
void Loader::print()
{
	for(auto it = prgms.begin(); 
			it != prgms.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
}
