#include "./headers/cpu/cpu.h"

CPU::CPU(Memory &memory, std::mutex& mtx, std::string& accumulator,
std::string& job_id, 
std::queue<PCB*>& io_queue, 
std::vector<PCB>& page_fault_queue, 
std::vector<std::string>& finished_jobs_queue) : 
memory(memory), 
job_id(job_id), io_queue(io_queue), 
page_fault_queue(page_fault_queue),
finished_jobs_queue(finished_jobs_queue)
{
  this->mtx = &mtx;
  this->accumulator = &accumulator;
  running_stack.push(-1000);
}

CPU::~CPU() 
{
  delete running;
  delete mtx;
  delete accumulator;
}

void CPU::run(PCB &pcb)
{
  std::cout << "Running..." << std::endl;
  t = clock();
  // busy cpu
  set_busy();
  set_running(pcb);
  job_id = running->job_id;
  instructions_size = running->instructions_size();
  running->state.set_running();
  // add job to running stack
  if (!running_stack.empty()) running_stack.pop();
  running_stack.push(hex_to_decimal(job_id));
  // load cache
  load_cache();
  // execution loop
  while (!running->state.waiting && running->state.running 
         && !running->state.terminated)
  {
    fetch();
    decode();
    execute();
  }
  // std::cout << "Finished Running..." << std::endl;
}

void CPU::load_cache()
{
  // 1. If new set capacity
  if (running->new_pcb)
    running->state.cache.set_capacity(running->total_pages * 4);
  // 2. Retrieve valid frames from ram and put them in cache
  for (int i = 0;  i < running->page_table.size(); i++)
  {
    if (!(running->page_table.retrieved(i)) && running->page_table.valid(i))
    {
      unsigned int frame_number = running->page_table.frame_number(i);
      running->state.cache.insert(memory.memory_manager.ram.retrieve(frame_number));
      running->page_table.set_retrieved(i, true);
    }
  }
  running->page_table.print();
  
  running->retrieved_frames = running->frames_assigned; 
  std::string fname = "cache_" + (std::to_string(hex_to_decimal(running->job_id))) + ".txt";
  running->state.cache.print(fname, hex_to_decimal(running->job_id)); 
}
void CPU::fetch() 
{
  //logfile << "Fetching..." << std::endl;
  // std::cout << "fetching..." << std::endl;
  try {
    running->registers.instruction = running->state.cache.fetch(running->program_counter );
    running->program_counter  += 1;
  } catch(...) {
    // std::cout << "Address: " << running->program_counter  << std::endl;
    int pc = running->program_counter ;
    unsigned int missing_frame = running->program_counter  / 4;
    if(running->program_counter  == running->instructions_size() && 
      !(running->program_counter  % 2 == 0))
      missing_frame += 1;
    add_running_to_page_fault_queue(missing_frame, pc);
    terminate();
  }
}

void CPU::decode()
{
  std::string bin_instruction = instruction_as_binary();
  running->registers.opcode = Decoder::retrieve_bits(bin_instruction, 2, 8);
  
  if (action_to_perform(bin_instruction) == "00")
    Decoder::arithmetic(running->registers, bin_instruction, 8);
  else if(action_to_perform(bin_instruction) == "01")
    Decoder::conditional(running->registers, bin_instruction, 8);
  else if(action_to_perform(bin_instruction) == "10")
    Decoder::unconditional(running->registers, bin_instruction, 8);
  else if(action_to_perform(bin_instruction) == "11")
    Decoder::io(running->registers, bin_instruction, 8);
}

void CPU::execute()
{
  std::string instruction = Conversions::binary_to_hex(running->registers.opcode);
  Call(instruction);
}

void CPU::terminate()
{
  save_state();
  reset_page_number();
  reset_program_counter();
  running->state.set_terminated();
}

void CPU::interrupt()
{
  save_state();
  running->new_pcb = false;
  reset_page_number();
  reset_program_counter();
  running->state.set_waiting();
}


void CPU::set_ram_addresses(int start, int end)
{
  start_address_in_ram = start;
  end_address_in_ram = end;
}

void CPU::save_state()
{
  //running->program_counter = program_counter;
}

void CPU::restore_state() 
{
  //program_counter = running->program_counter;
}

int CPU::direct_effective_address()
{
  int address = binary_to_decimal(running->registers.address);
  return address / 4;
}

std::string CPU::instruction_as_binary() 
{ 
  return Conversions::hex_to_binary(running->registers.instruction);    
}

std::string CPU::action_to_perform(std::string &binary_instruction)
{
  return binary_instruction.substr(0, 2);
}

void CPU::Call(std::string& instruct)
{
  auto itr = instuct_map.find(instruct);
  if(itr != instuct_map.end())
  {
    IFM f = instuct_map[instruct];
    (this->*f)();    
  }
}

// =============== Instruction Set ========================================
void CPU::rd() {
  int address = binary_to_decimal(running->registers.address);
  if (address == 0)
  {
    running->registers.address = 
      running->registers.reg[running->registers.in_out_r2];
    
    int missing_frame = direct_effective_address() / 4;
    if(!(running->instructions_size() % 4 == 0))
        missing_frame += 1;
    
    if (running->page_table.retrieved(missing_frame))
    {  
      running->registers.reg[running->registers.in_out_r1] = 
          hex_to_binary(running->state.cache.fetch(direct_effective_address()));
    }
    else
    {
      add_running_to_page_fault_queue(missing_frame, direct_effective_address());
    }
  }
  else
  {
    int missing_frame = direct_effective_address() / 4;
    if(!(running->instructions_size() % 4 == 0))
        missing_frame += 1;
    
    if (running->page_table.retrieved(missing_frame))
    { 
      running->registers.reg[running->registers.in_out_r1] = 
          hex_to_binary(running->state.cache.fetch(direct_effective_address()));
    }
    else
    {
      add_running_to_page_fault_queue(missing_frame, direct_effective_address());
    }
  }
}

void CPU::wr() 
{
  int address = 
   binary_to_decimal(running->registers.reg[running->registers.in_out_r2]);
  if(address > 0)
  {
    running->registers.in_out_r2 = running->registers.in_out_r1;
  }
  else
  {
    int missing_frame = direct_effective_address() / 4;
    if(!(running->instructions_size() % 4 == 0))
        missing_frame += 1;
    if (running->page_table.retrieved(missing_frame))
    {
      running->state.cache.write(direct_effective_address(), 
        running->registers.reg[running->registers.in_out_r1]);
    }
    else
    {
      add_running_to_page_fault_queue(missing_frame, direct_effective_address());
    }
  }
}

void CPU::st()
{
  int address = binary_to_decimal(running->registers.address);
  if (address == 0)
  {
    running->registers.address = 
                          running->registers.reg[running->registers.destination];
    
    int missing_frame = direct_effective_address() / 4;
    if(!(running->instructions_size() % 4 == 0))
        missing_frame += 1;
    if (running->page_table.retrieved(missing_frame))
    {
      std::string content = 
                 binary_to_hex(running->registers.reg[running->registers.base]);
      running->state.cache.write(direct_effective_address(), content);
    }
    else
    {
      add_running_to_page_fault_queue(missing_frame, direct_effective_address());
    }
  }
  else 
  {
    int missing_frame = direct_effective_address() / 4;
    if(!(running->instructions_size() % 4 == 0))
        missing_frame += 1;
    if (running->page_table.retrieved(missing_frame))
    {
      std::string content = 
        binary_to_hex(running->registers.reg[running->registers.destination]);
      running->state.cache.write(direct_effective_address(), content);
    }
    else
    {
      add_running_to_page_fault_queue(missing_frame, direct_effective_address());
    }
  }
}

void CPU::lw()
{
  int base = binary_to_decimal(running->registers.reg[running->registers.base]); // 220
  int address = base / 4 + binary_to_decimal(running->registers.address);
  
  int missing_frame = direct_effective_address() / 4;
  if(!(running->instructions_size() % 4 == 0))
      missing_frame += 1;

  if (running->page_table.retrieved(missing_frame))
  {
    std::string content = running->state.cache.fetch(address);
    content = decimal_to_binary(hex_to_decimal(content));
    running->registers.reg[running->registers.destination] = content;
  }
  else
  {
    add_running_to_page_fault_queue(missing_frame, direct_effective_address());
  }
}

void CPU::mov()
{
  running->registers.reg[running->registers.first_source] = 
    running->registers.reg[running->registers.second_source];
}

void CPU::add()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.first_source])
                   + binary_to_decimal(running->registers.reg[running->registers.second_source]));
}

void CPU::sub()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.first_source])
                   - binary_to_decimal(running->registers.reg[running->registers.second_source]));
}

void CPU::mul()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.first_source])
                   * binary_to_decimal(running->registers.reg[running->registers.second_source]));
  
}

void CPU::div_op()
{
  //logfile << "div: dividing..." << std::endl;
  int second_source = binary_to_decimal(running->registers.reg[running->registers.second_source]);
  if (second_source == 0) {
    //logfile << "Error!! Dividing by 0!" << std::endl;
    terminate();
  }
  else
  {
    running->registers.reg[running->registers.destination] = 
    decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.first_source])
                     / binary_to_decimal(running->registers.reg[running->registers.second_source]));
  }
}

void CPU::and_op(){ }
void CPU::or_op(){}

void CPU::movi()
{
  running->registers.reg[running->registers.destination] = running->registers.address;
}

void CPU::addi()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.destination])
                   + binary_to_decimal(running->registers.address));
}

void CPU::muli()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.destination])
                   * binary_to_decimal(running->registers.address));
}

void CPU::divi()
{
  running->registers.reg[running->registers.destination] = 
  decimal_to_binary (binary_to_decimal(running->registers.reg[running->registers.destination])
                   / binary_to_decimal(running->registers.address));
}

void CPU::ldi()
{
  running->registers.reg[running->registers.destination] = running->registers.address;
} 

void CPU::slt()
{
  int s1 = binary_to_decimal(running->registers.reg[running->registers.first_source]);
  int s2 = binary_to_decimal(running->registers.reg[running->registers.second_source]);

  if (s1 < s2)
    running->registers.reg[running->registers.destination] = decimal_to_binary(1);
  else
    running->registers.reg[running->registers.destination] = decimal_to_binary(0);
}

void CPU::slti()
{
  int first_source = binary_to_decimal(running->registers.reg[running->registers.first_source]);
  if (first_source < direct_effective_address())
    running->registers.reg[running->registers.destination] = decimal_to_binary(1);
  else
    running->registers.reg[running->registers.destination] = decimal_to_binary(0);
}


void CPU::hlt()
{
  for(auto it = running->page_table.table.begin(); 
           it != running->page_table.table.end();it++)
  {
    if (it->second.isRetrieved() && it->second.isValid())
    {
      int frame = it->second.get_frame_number();
      memory.memory_manager.ram.insert_free_frame(frame);
    }
  }
  
  std::string output = "Job: " + std::to_string(hex_to_decimal(job_id));
  std::string value = std::to_string(binary_to_decimal(running->registers.reg["0000"]));
  output += " Output: " + value;
  finished_jobs_queue.push_back(output);
    
  terminate();
}

void CPU::jmp()
{
  int address = direct_effective_address();
  running->program_counter  = address;
}

void CPU::beq()
{
  //logfile << "be: branching since B-reg = D-reg..." << std::endl;
  int base = binary_to_decimal(running->registers.reg[running->registers.base]);
  int destination = binary_to_decimal(running->registers.reg[running->registers.destination]);
  if (base == destination)
    running->program_counter  = direct_effective_address();
} 

void CPU::bne()
{
  //logfile << "bne: branching if B-reg != D-reg..." << std::endl;
  int base = binary_to_decimal(running->registers.reg[running->registers.base]);
  int destination = binary_to_decimal(running->registers.reg[running->registers.destination]);
  if (base != destination)
    running->program_counter  = direct_effective_address();
}

void CPU::bez() { }
void CPU::bnz() { }
void CPU::bgz() { }
void CPU::blz() { }
void CPU::nop() { }

void CPU::add_running_to_io_queue()
{
  running->state.set_waiting();
  io_queue.push(running);
  interrupt();
}

void CPU::add_running_to_page_fault_queue
(unsigned int missing_frame, unsigned int address)
{
  running->state.missing_frame = missing_frame;
  running->state.missing_address = address;
  running->state.set_waiting();
  running->new_pcb = false;
  running->program_counter -= 1;
  page_fault_queue.emplace_back(*running);
}

void CPU::print_log(std::string output_file_name, std::string content)
{
  std::ofstream logger;
  logger.open (output_file_name, std::ios_base::app);
  logger << content << std::endl;
  logger.close();
  logger.clear();
}

void CPU::print_registers()
{
  std::cout << "====== running->registers =========" << std::endl;
  for(auto it: running->registers.reg)
  {
    if(it.second != "")
    {
      std::cout <<  binary_to_decimal(it.first) << "\t" <<
                    it.first << "\t" << 
                    binary_to_decimal(it.second) << std::endl;
    }
    else
    {
      std::cout <<  binary_to_decimal(it.first) << "\t" <<
                    it.first << "\t" << "X" << std::endl;
    } 
  }
  std::cout << "=============================" << std::endl;
}

void CPU::init_instructions() 
{
  instuct_map.insert(std::make_pair("0", &CPU::rd)); 
  instuct_map.insert(std::make_pair("1", &CPU::wr)); 
  instuct_map.insert(std::make_pair("2", &CPU::st)); 
  instuct_map.insert(std::make_pair("3", &CPU::lw)); 
  instuct_map.insert(std::make_pair("4", &CPU::mov)); 
  instuct_map.insert(std::make_pair("5", &CPU::add)); 
  instuct_map.insert(std::make_pair("6", &CPU::sub)); 
  instuct_map.insert(std::make_pair("7", &CPU::mul)); 
  instuct_map.insert(std::make_pair("8", &CPU::div_op)); 
  instuct_map.insert(std::make_pair("9", &CPU::and_op)); 
  instuct_map.insert(std::make_pair("a", &CPU::or_op)); 
  instuct_map.insert(std::make_pair("b", &CPU::movi)); 
  instuct_map.insert(std::make_pair("c", &CPU::addi)); 
  instuct_map.insert(std::make_pair("d", &CPU::muli)); 
  instuct_map.insert(std::make_pair("e", &CPU::divi)); 
  instuct_map.insert(std::make_pair("f", &CPU::ldi)); 
  instuct_map.insert(std::make_pair("10", &CPU::slt)); 
  instuct_map.insert(std::make_pair("11", &CPU::slti)); 
  instuct_map.insert(std::make_pair("12", &CPU::hlt)); 
  instuct_map.insert(std::make_pair("13", &CPU::nop)); 
  instuct_map.insert(std::make_pair("14", &CPU::jmp)); 
  instuct_map.insert(std::make_pair("15", &CPU::beq)); 
  instuct_map.insert(std::make_pair("16", &CPU::bne)); 
  instuct_map.insert(std::make_pair("17", &CPU::bez)); 
  instuct_map.insert(std::make_pair("18", &CPU::bnz)); 
  instuct_map.insert(std::make_pair("19", &CPU::bgz)); 
  instuct_map.insert(std::make_pair("1a", &CPU::blz));   
}