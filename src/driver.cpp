#include <iostream>
#include <cstring>
#include <cassert>

#include "./headers/cpu/cpu.h"
#include "./headers/cpu/dispatcher/dispatcher.h"
#include "./headers/memory/memory.h"
#include "./headers/schedulers/schedulers.h"

using namespace std;
int main()
{

  Memory memory("./src/Program-File.txt");
  memory.load();
  Dispatcher dispatcher(memory);
  Schedulers schedulers(memory, dispatcher);
  schedulers.schedule();
  cout << "Success!" << endl;
}